import datetime


class Stopwatch:

    def __init__(self):
        self.__start_time = None
        self.__end_time = None
        self.__elapsed_seconds = None


    def start(self):
        self.__start_time = datetime.datetime.now()


    def stop(self):
        self.__end_time = datetime.datetime.now()


    def elapsed(self):
        duration = self.__end_time - self.__start_time
        self.__elapsed_seconds = duration.total_seconds()
        return int(self.__elapsed_seconds)