class LocoER:
    def __init__(self, iid, loco_name):
        self.intellitrain_id = iid
        self.loco_id = loco_name
        self.files = None

class LocoFile:
    def __init__(self, iid, date, name, dl_link):
        self.loco_intellitrain_id = iid
        self.creation_date = date
        self.file_name = name
        self.download_link = dl_link

