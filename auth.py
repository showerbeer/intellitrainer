__author__ = 'Christian'

import configparser
import logging


def read_ini(file_name, section):
    parser = configparser.ConfigParser()
    parser.read(file_name)
    lines = parser.options(section)
    dict1 = {}
    for line in lines:
        try:
            dict1[line] = parser.get(section, line)
        except IOError:
            logging.error('(read_ini) Failed to read a value for key', line)
            dict1[line] = None
    return dict1