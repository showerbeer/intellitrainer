from bs4 import BeautifulSoup
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from os.path import isfile, join
import os
from random import randint
import logging
import datetime
import re
import requests
import ldap_connector
import sys
import auth
import stopwatch
import classes

# Turn off SSL warnings. Asciano proxy doesn't like to validate things
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# Configure logging
LOG_PATHS = auth.read_ini('config.ini', 'logging')
LOG_PATH = LOG_PATHS['log_file_path']

# Turn off info logging from requests
logging.getLogger("urllib3").setLevel(logging.WARNING)
logging.getLogger("requests").setLevel(logging.WARNING)

# Configure logger
logging.basicConfig(filename=LOG_PATH,
                    level=logging.INFO,
                    format='%(asctime)s | %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')

CREDENTIALS = auth.read_ini('credentials.ini', 'credentials')
PATHS = auth.read_ini('config.ini', 'paths')
PROXY_USER = CREDENTIALS['proxy_username']
PROXY_PASS = CREDENTIALS['proxy_password']
LOGIN_BUTTON_X = randint(4, 11)
LOGIN_BUTTON_Y = randint(1, 4)
INTELLITRAIN_USER = CREDENTIALS['intellitrain_username']
INTELLITRAIN_PASS = CREDENTIALS['intellitrain_password']
DOWNLOAD_PATH = PATHS['download_path']
MAP = {}
REQUEST_HEADERS = {
    "Accept-Encoding": "gzip,deflate",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0"}
LOGIN_URL = 'https://intellitrain.emdiesels.com/Login.aspx'
PROXIES = {
    'http': 'http://{proxy_user}:{proxy_pass}@proxy.gslb.asciano.ad:8080/'.format(proxy_user=PROXY_USER,
                                                                                  proxy_pass=PROXY_PASS),
    'https': 'https://{proxy_user}:{proxy_pass}@proxy.gslb.asciano.ad:8080/'.format(proxy_user=PROXY_USER,
                                                                                  proxy_pass=PROXY_PASS)
}

def get_post_data_login(login_soup):
    return {
        "__EVENTTARGET": "",
        "__EVENTARGUMENT": "",
        "__EVENTVALIDATION": login_soup.find('input', id='__EVENTVALIDATION')['value'],
        "__VIEWSTATE": login_soup.find('input', id='__VIEWSTATE')['value'],
        "__VIEWSTATEGENERATOR": login_soup.find('input', id='__VIEWSTATEGENERATOR')['value'],
        "UserName": INTELLITRAIN_USER,
        "Password": INTELLITRAIN_PASS,
        "_ctl9": "Log in",
        "TimeZoneOffset": ""
    }


# Read in loco mapping
with open('loco-mapping.csv') as f:
    for line in f:
        l = line.split(',')
        MAP[l[0]] = int(l[1])


# Check for already downloaded files
existing_files = os.listdir(DOWNLOAD_PATH)

# Start a timer, and count total downloads
downloads = 0
timer = stopwatch.Stopwatch()
timer.start()

# Create Session using 'with' so it's disposed and closed after
with requests.Session() as _session:
    page = _session.get(LOGIN_URL, headers=REQUEST_HEADERS, proxies=PROXIES, verify=False)
    html = page.content

    # Make login-page soup
    login_soup = BeautifulSoup(html, 'html.parser')

    # Get post data
    post_data = get_post_data_login(login_soup)

    # Perform log-in
    print('Logging in...')
    page = _session.post(LOGIN_URL, data=post_data, proxies=PROXIES, verify=False)

    # Print log-in page status code
    print('Status code: ', page.status_code)

    if page.status_code == 200:
        print('Log-in successful')
        logging.info('Logged in to Intellitrain. Begin downloading')
    else:
        print('Could not log in. Exiting..')
        logging.error('Could not log in to Intellitrain website!')
        sys.exit()

    # Get fleet soup
    fleet = BeautifulSoup(page.content, 'html.parser')

    # Get all locos
    locos = []
    for table_row in fleet('tr', align='center'):
        table_cell = table_row('td')
        loco = classes.LocoER(MAP[table_cell[0].get_text().strip()],
                              table_cell[0].get_text().strip())
        locos.append(loco)

    # Sort the array by loco name
    locos = sorted(locos, key=lambda l:l.loco_id)

    # Download all files for every loco in fleet summary
    for l in locos:
        # Fetch all recently loaded files from LDAP
        loaded_files = ldap_connector.get_file_list(l.loco_id)

        # Get loco ID
        intellitrain_id = l.intellitrain_id

        logging.info('Fetching page for locomotive {loco}'.format(loco=l.loco_id))

        # Create event recorder URL
        locoURL = 'https://intellitrain.emdiesels.com/LocomotiveEventRecorder.aspx?LocomotiveID={key}&FleetID=0'.format(key=intellitrain_id)

        # Attempt to get a loco page
        lf = _session.get(locoURL, proxies=PROXIES, verify=False)

        # Make loco soup
        loco_soup = BeautifulSoup(lf.content, 'html.parser')

        # Collect a list of File class objects about each rows in the table
        files = []
        for table_row in loco_soup('tr', align='center'):
            table_cell = table_row('td')

            # Date the file was created
            file_date = datetime.datetime.strptime(table_cell[0].get_text().strip(), '%m/%d/%y %I:%M:%S %p')

            # Name of the binary log file
            file_name = table_cell[2].get_text().strip()

            # Don't re-download files we already have
            if file_name not in existing_files:

                # Extract the link from the row using soup.tag.element['attribute']
                link = table_cell[3].a['href']

                # Use regex to find the download link
                dl_link = re.search('LocomotiveFIRE_EventRecorderGrid:_ctl[0-9]+:Download', link).group(0)
                file = classes.LocoFile(table_cell[0].get_text().strip(), file_date, file_name, dl_link)
                files.append(file)
            
            else:
                logging.info('File already downloaded, skipping ({fn})'.format(fn=file_name))

        # Get some properties from the page needed for downloading files
        hours_of_download = loco_soup.find('input', id='HoursOfDownload')['value']
        from_date = loco_soup.find('input', id='DataFilter1_FromDate')['value']
        to_date = loco_soup.find('input', id='DataFilter1_ToDate')['value']
        event_validation = loco_soup.find('input', id='__EVENTVALIDATION')['value']
        view_state = loco_soup.find('input', id='__VIEWSTATE')['value']
        view_state_generator = loco_soup.find('input', id='__VIEWSTATEGENERATOR')['value']

        # Download for each file for the loco
        new_files = [x for x in files if x.file_name not in loaded_files]

        logging.info('Locomotive {loco} has {num} new logs to download (out of {tot} total)'.format(
            loco=l.loco_id, num=len(new_files), tot=len(files)
        ))

        for file in new_files:
            s = file.download_link

            # Check if file has already been loaded
            if file.file_name in loaded_files:
                logging.warning('I just attempted to download a file already listen in dbo.LoadedFiles\nThis shouldn\'t happen!')
                continue

            # Download each file in order with some delay
            dl_post_data = {
                "__EVENTTARGET": s,
                "__EVENTARGUMENT": "",
                "__LASTFOCUS": "",
                "__EVENTVALIDATION": event_validation,
                "__VIEWSTATE": view_state,
                "__VIEWSTATEGENERATOR": view_state_generator,
                "Selectlocomotive1:FleetsDropDownList": -1,
                "Selectlocomotive1:LocomotivesDropDownList": l.intellitrain_id,
                "DataFilter1:FromDate": '08/10/2017',
                "DataFilter1:ToDate": to_date,
                "DataFilter1:Amount": "",
                "DataFilter1:TimeUnits": "Days",
                "HoursOfDownload": hours_of_download
            }
            
            try:
                download_request = _session.post(locoURL, data=dl_post_data, proxies=PROXIES, verify=False)

                dl_path = DOWNLOAD_PATH + file.file_name
                logging.info('Downloading file: {path}'.format(path=dl_path))
                with open(dl_path, 'wb') as wb:
                    wb.write(download_request.content)

                downloads = downloads + 1
            except Exception as e:
                logging.error('Exception occurred when attempting to download file')
                logging.error(e)

        logging.info('Finished downloading {loco}'.format(loco=l.loco_id))


timer.stop()
seconds = timer.elapsed()
minutes = int(seconds / 60) % 60
hours = int(seconds / 3600)
logging.info('Completed {dls} downloads in {h} hours, {m} minutes and {s} seconds'.format(
    dls=downloads, h=hours, m=minutes, s=seconds%60))
logging.info('===========================================================================================')
