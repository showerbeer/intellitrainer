# Intellitrainer

## Automated downloading of Intellitrain log files

### Configuration
* Use the `credentials.ini.example` as a template to create your credentials file
* Enter your proxy authentication and intellitrain user details in `credentials.ini`

### Dependencies
##### Hint: Use [pip](https://pypi.python.org/pypi/pip) to download and install
* BeautifulSoup4
* Requests
* configparser
* pypyodbc (SQL Server connector)
* urllib3 (python standard library)
* re (python standard library)
* os (python standard library)

### Todo
* Command line arguments for specifying
    * locomotives or class of locomotives
    * date ranges for downloads
* Sort files by loco class and month/year??