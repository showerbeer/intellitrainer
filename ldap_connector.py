import pypyodbc
import auth
import logging


def get_file_list(loco_name):
    ldap = auth.read_ini('config.ini', 'ldap')
    connection_string = ldap['connection_string']
    connection = pypyodbc.connect(connection_string)

    try:
        cursor = connection.cursor()
        #logging.info('Executing SQL query for file names')
        sql = """
             Select txRawFile 
             From dbldap.dbo.LoadedFiles LF 
             Inner Join dbldap.dbo.MD_Locomotives L 
             On LF.nLocoSID = L.nSID 
             Where L.txIntelliTrainLocoId = ? 
             And Isnull(dtGMTmin, dtGMT) > DATEADD(month, -24, GETDATE())"""

        # Tyler Haigh - 2017-05-05 - Update from "And dtGmt > ..." to "And dtGmtmin >" due to old files appearing in intellitrain site
        # and NuVan redownloading them
        # This will now download any loaded files whose data range starts within the last n months
        # Extending date range to 2 years since we are seeing files that are more than 2 months old

        # Execute the query
        cursor.execute(sql, (loco_name, ))

        count = 0
        file_list = []
        for row in cursor:
            count = count + 1
            file_list.append(row[0])

        #logging.info('Finished SQL query. Found {num} recently loaded files for {loco_name} in LDAP'.format(num=count, loco_name=loco_name))

        # Close connection
        cursor.close()

        return file_list
    except Exception as e:
        logging.error('Something went wrong when polling LDAP')
        logging.error(e)
    finally:
        connection.close()
